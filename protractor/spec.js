
describe('Protractor Test Site', function() {

  // it('should have a title', function() {

  //   var title = 'Lynda: Online Courses, Classes, Training, Tutorials';
  //   var EC = protractor.ExpectedConditions;
  //   var width = 800;
  //   var height = 600;

  //   // browser.waitForAngularEnabled(false);

  //   browser.driver.manage().window().setSize(width, height);

  //   browser.get('https://www.lynda.com');

  //   expect(browser.getTitle()).toEqual(title);

  //   browser.sleep('6000');
  //   browser.waitForAngularEnabled(true);

  // });

  it('should have title', function() {

    var width = 800;
    var height = 600;
    var title = 'Nasdaq Stock Market | Stock Quotes & Stock Exchange News';
    browser.waitForAngularEnabled(false);

    browser.get('http://www.nasdaq.com');
    browser.driver.manage().window().setSize(width, height);
    expect(browser.getTitle()).toEqual(title);
  });

  it('should search epam', function() {
    // browser.get('https://www.lynda.com');
    element(by.id('toggle-search')).click();
    element(by.id('header-search-field')).sendKeys('protractor');
    element(by.id('header-search-field')).sendKeys(protractor.Key.ENTER);
    browser.sleep('6000');
  });


});

