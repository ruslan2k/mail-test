
describe('Nasdaq.com Test', function() {

  var EC = protractor.ExpectedConditions;
  var stockQuotes = [
    {code: 'epam', title: 'EPAM', url: 'http://www.nasdaq.com/symbol/epam/real-time'},
    {code: 'goog', title: 'Alphabet', url: 'http://www.nasdaq.com/symbol/goog/real-time'},
    {code: 'fb', title: 'Facebook', url: 'http://www.nasdaq.com/symbol/fb/real-time'},
  ];

  it('should have title', function() {
    var width = 800;
    var height = 768;
    var title = 'Nasdaq Stock Market | Stock Quotes & Stock Exchange News';

    browser.waitForAngularEnabled(false);
    browser.driver.manage().window().setSize(width, height);

    browser.get('http://www.nasdaq.com');
    expect(browser.getTitle()).toEqual(title);
  });

  stockQuotes.forEach(function(stock) {
    var code = stock.code;
    var title = stock.title;
    var url = stock.url;

    it('should search ' + title, function() {
      // console.log(code);

      element(by.id('stock-search-text')).sendKeys(code);
      element(by.id('stock-search-text')).sendKeys(protractor.Key.ENTER);
      browser.sleep('3000');

      browser.wait(EC.urlIs(url), 3000);
      browser.wait(EC.titleContains(title), 3000);
    });
  });

  it('should display graphs', function() {
    var epamUrl = stockQuotes[0].url;
    element(by.css(`a[href="${epamUrl}"]`)).click();
    browser.sleep('3000');

    browser.wait(EC.urlIs(epamUrl), 3000);
  });

});

