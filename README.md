# CDP

## Prerequisites

node.js installed

firefox installed

chrome installed

java installed

## Installing instruction

http://webdriver.io/guide.html

http://www.protractortest.org/


```
npm install
webdriver-manager update
webdriver-manager start
```

## Protractor

Test Nasdaq site

```
cd protractor
protractor config.js
```

## Webdriver JS

Pre. Manually create 2 email accounts on yndex mail.
Code.
1.	Login into account 1;
2.	Send email to account 2. Email body is up to you;
3.	Check presence of e-mail in Sent folder;
4.	Login into account 2;
5.	Check presence of email sent from account 1.


### Running the test
```
cd wd
# Create .env.js file with two yandex accounts
cp .env.js-example .env.js
$EDITOR .env.js

# Run test
./node_modules/.bin/wdio wdio.conf.js --waitforTimeout=30000
```
