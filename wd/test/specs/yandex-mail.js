var Access = require('../../.env.js')

var assert = require('assert');
const uuidv1 = require('uuid/v1');

var subj = Access.subjPrefix + uuidv1();

describe('Yandex mail for the Sender', function() {

  it('should show you log in screen', function() {
    browser.url('https://mail.yandex.com/');
    browser.waitForExist('=Log in');
    browser.click('=Log in');
    var pageTitle = browser.getTitle();
    assert.equal(pageTitle, 'Authorization');
  });

  it('should let you pass authorization', function() {
    browser.pause(60000);
    browser.waitForExist('input[name="login"]');
    browser.setValue('[name="login"]', Access.fromUser);
    browser.setValue('[name="passwd"]', Access.fromPasswd);
    browser.click('input[name="twoweeks"]');
    browser.click('.passport-Button');
    browser.waitForExist('.mail-ComposeButton');
  });

  it('should let you compose mail', function() {
    browser.waitUntil(function() {
      return browser.getUrl().indexOf('#inbox') >= 0;
    });

    browser.waitForExist('.mail-ComposeButton');
    browser.click('a.mail-ComposeButton');

    browser.waitForVisible('[name="subj"]');
    browser.setValue('[name="subj"]', subj);

    browser.waitForVisible('[name="to"]');
    browser.setValue('[name="to"]', Access.toUser);

    browser.click('textarea[role="textbox"]');
    browser.setValue('textarea[role="textbox"]', 'This is the test message');

    browser.click('button[type="submit"]');
    browser.pause(20000);
    browser.click('[href="#sent"]');
  });

  it('should let you check mail in sent folder', function() {
    browser.waitUntil(function() {
      //console.log(browser.getUrl());
      return browser.getUrl().indexOf('#sent') >= 0;
    }, 60000, 'expected mail was sent');
    browser.waitForExist('span[title="'+ subj +'"]');
    browser.url('https://passport.yandex.com/');
  });

  it('should let you log out', function() {
    browser.waitUntil(function() {
      return browser.getUrl().indexOf('passport') >= 0;
    });
    browser.click('.header-user-avatar');
    browser.waitForVisible('=Log out');
    browser.click('=Log out');
  });
});


describe('Yandex mail for the Recipient', function() {

  it('should let you pass authorization', function() {
    browser.url('https://mail.yandex.com/');
    browser.waitForExist('input[name="login"]');
    browser.setValue('[name="login"]', Access.toUser);
    browser.setValue('[name="passwd"]', Access.toPasswd);
    browser.click('input[name="twoweeks"]');
    browser.click('.passport-Button');
  });

  it('should show you inbox and mail', function() {
    browser.waitForExist('[href="#inbox"]');
    browser.click('[href="#inbox"]');
    browser.waitForExist('span[title="'+ subj +'"]');
  });


});

